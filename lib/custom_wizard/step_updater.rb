# frozen_string_literal: true
class CustomWizard::StepUpdater
  include ActiveModel::Model

  attr_accessor :refresh_required, :submission, :result, :step

  def initialize(current_user, wizard, step, submission, save_only = false)
    puts "StepUpdaterInitialize"
    @current_user = current_user
    @wizard = wizard
    @step = step
    @refresh_required = false
    @submission = submission.to_h.with_indifferent_access
    @result = {}
    @save_only = save_only
  end

  def update
    puts "STEP UPDATER dot UPDATE called"
    if SiteSetting.custom_wizard_enabled &&
       @step.present? &&
       @step.updater.present? &&
       success?

      puts "BOOP"
      puts @step.inspect
      puts "BAAP"
      puts @step.updater.inspect
      puts self
      @step.updater.call(self) # --> CustomWizard::Builder save_submissions
      puts "BEEP"

      UserHistory.create(
        action: UserHistory.actions[:custom_wizard_step],
        acting_user_id: @current_user.id,
        context: @wizard.id_category,
        subject: @step.id
      )
    else
      false
    end
    puts "end STEP UPDATER dot UPDATE"
  end

  def updateFields
    puts "STEP UPDATER dot UPDATEFIELDS START"
    if SiteSetting.custom_wizard_enabled &&
       @step.present? &&
       @step.updater.present? &&
       success?

      puts "BOOOOO"
      puts @step.inspect
      puts "BAAAAA"
      puts @step.updater.inspect
      @step.updater.call(self) # <-- this does the update
      puts "BEEEEE"
    else
      false
    end
    puts "STEP UPDATER dot UPDATEFIELDS END"
  end

  def success?
    @errors.blank?
  end

  def refresh_required?
    @refresh_required
  end

  def save_only?
    @save_only
  end

  def validate
    CustomWizard::UpdateValidator.new(self).perform unless @save_only
  end
end
