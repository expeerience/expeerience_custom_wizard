# frozen_string_literal: true
# name: expeerience_custom_wizard
# about: Create custom wizards
# version: 0.7.0
# authors: Angus McLeod
# url: https://github.com/paviliondev/expeerience_custom_wizard
# contact emails: angus@thepavilion.io

register_asset 'stylesheets/common/wizard-admin.scss'
register_asset 'stylesheets/common/wizard-mapper.scss'
register_asset 'lib/jquery.timepicker.min.js'
register_asset 'lib/jquery.timepicker.scss'

enabled_site_setting :custom_wizard_enabled

config = Rails.application.config
plugin_asset_path = "#{Rails.root}/plugins/expeerience_custom_wizard/assets"
config.assets.paths << "#{plugin_asset_path}/javascripts"
config.assets.paths << "#{plugin_asset_path}/stylesheets/wizard"

if Rails.env.production?
  config.assets.precompile += %w{
    wizard-custom-guest.js
    wizard-custom-globals.js
    wizard-custom.js
    wizard-custom-start.js
    wizard-plugin.js.erb
    wizard-raw-templates.js.erb
  }
end

if respond_to?(:register_svg_icon)
  register_svg_icon "far-calendar"
  register_svg_icon "chevron-right"
  register_svg_icon "chevron-left"
  register_svg_icon "save"
end

after_initialize do
  %w[
    ../lib/custom_wizard/engine.rb
    ../config/routes.rb
    ../controllers/custom_wizard/admin/admin.rb
    ../controllers/custom_wizard/admin/wizard.rb
    ../controllers/custom_wizard/admin/submissions.rb
    ../controllers/custom_wizard/admin/api.rb
    ../controllers/custom_wizard/admin/logs.rb
    ../controllers/custom_wizard/admin/manager.rb
    ../controllers/custom_wizard/admin/custom_fields.rb
    ../controllers/custom_wizard/wizard.rb
    ../controllers/custom_wizard/steps.rb
    ../controllers/custom_wizard/realtime_validations.rb
    ../jobs/clear_after_time_wizard.rb
    ../jobs/refresh_api_access_token.rb
    ../jobs/set_after_time_wizard.rb
    ../lib/custom_wizard/validators/template.rb
    ../lib/custom_wizard/validators/update.rb
    ../lib/custom_wizard/action_result.rb
    ../lib/custom_wizard/action.rb
    ../lib/custom_wizard/builder.rb
    ../lib/custom_wizard/cache.rb
    ../lib/custom_wizard/custom_field.rb
    ../lib/custom_wizard/field.rb
    ../lib/custom_wizard/realtime_validation.rb
    ../lib/custom_wizard/realtime_validations/result.rb
    ../lib/custom_wizard/realtime_validations/similar_topics.rb
    ../lib/custom_wizard/mapper.rb
    ../lib/custom_wizard/log.rb
    ../lib/custom_wizard/step_updater.rb
    ../lib/custom_wizard/template.rb
    ../lib/custom_wizard/wizard.rb
    ../lib/custom_wizard/api/api.rb
    ../lib/custom_wizard/api/authorization.rb
    ../lib/custom_wizard/api/endpoint.rb
    ../lib/custom_wizard/api/log_entry.rb
    ../serializers/custom_wizard/api/authorization_serializer.rb
    ../serializers/custom_wizard/api/basic_endpoint_serializer.rb
    ../serializers/custom_wizard/api/endpoint_serializer.rb
    ../serializers/custom_wizard/api/log_serializer.rb
    ../serializers/custom_wizard/api_serializer.rb
    ../serializers/custom_wizard/basic_api_serializer.rb
    ../serializers/custom_wizard/basic_wizard_serializer.rb
    ../serializers/custom_wizard/custom_field_serializer.rb
    ../serializers/custom_wizard/wizard_field_serializer.rb
    ../serializers/custom_wizard/wizard_step_serializer.rb
    ../serializers/custom_wizard/wizard_serializer.rb
    ../serializers/custom_wizard/log_serializer.rb
    ../serializers/custom_wizard/realtime_validation/similar_topics_serializer.rb
    ../extensions/extra_locales_controller.rb
    ../extensions/invites_controller.rb
    ../extensions/users_controller.rb
    ../extensions/wizard_field.rb
    ../extensions/wizard_step.rb
    ../extensions/custom_field/preloader.rb
    ../extensions/custom_field/serializer.rb
  ].each do |path|
    load File.expand_path(path, __FILE__)
  end

  add_class_method(:wizard, :user_requires_completion?) do |user|
    wizard_result = self.new(user).requires_completion?
    return wizard_result if wizard_result

    custom_redirect = false

    if user &&
       user.first_seen_at.blank? &&
       wizard = CustomWizard::Wizard.after_signup(user)

      if !wizard.completed?
        custom_redirect = true
        CustomWizard::Wizard.set_wizard_redirect(wizard.id, user)
      end
    end

    !!custom_redirect
  end

  add_to_class(:users_controller, :wizard_path) do
    puts "HAHA HAHA HAHA HAHA HAHA HAHA HAHA HAHA "
    puts "HAHA HAHA HAHA HAHA HAHA HAHA HAHA HAHA "
    puts "HAHA HAHA HAHA HAHA HAHA HAHA HAHA HAHA "
    puts "HAHA HAHA HAHA HAHA HAHA HAHA HAHA HAHA "
    puts "HAHA HAHA HAHA HAHA HAHA HAHA HAHA HAHA "
    if custom_wizard_redirect = current_user.custom_fields['redirect_to_wizard']
      "#{Discourse.base_url}/w/#{custom_wizard_redirect.dasherize}"
    else
      "#{Discourse.base_url}/wizard"
    end
  end

  add_to_serializer(:current_user, :redirect_to_wizard) do
    object.custom_fields['redirect_to_wizard']
  end

  on(:user_approved) do |user|
    if wizard = CustomWizard::Wizard.after_signup(user)
      CustomWizard::Wizard.set_wizard_redirect(wizard.id, user)
    end
  end

  add_to_class(:application_controller, :redirect_to_wizard_if_required) do
    # puts "HELLO WORLD HELLO WORLD HELLO WORLD HELLO WORLD HELLO WORLD "
    # puts "HELLO WORLD HELLO WORLD HELLO WORLD HELLO WORLD HELLO WORLD "
    # puts "HELLO WORLD HELLO WORLD HELLO WORLD HELLO WORLD HELLO WORLD "
    # puts "HELLO WORLD HELLO WORLD HELLO WORLD HELLO WORLD HELLO WORLD "
    # puts "HELLO WORLD HELLO WORLD HELLO WORLD HELLO WORLD HELLO WORLD "
    puts "HELLO WORLD HELLO WORLD HELLO WORLD HELLO WORLD HELLO WORLD "
    puts "------------------------------------------------------------"
    wizard_id = current_user.custom_fields['redirect_to_wizard']
    puts "wizard id is #{wizard_id}"
    @excluded_routes ||= SiteSetting.wizard_redirect_exclude_paths.split('|') + ['/w/']
    url = request.referer || request.original_url
    puts "URL is #{url}"
    if request.format === 'text/html' && !@excluded_routes.any? { |str| /#{str}/ =~ url } && wizard_id
      puts "11111111"
      if request.referer !~ /\/w\// && request.referer !~ /\/invites\//
        puts "22222222"
        CustomWizard::Wizard.set_submission_redirect(current_user, wizard_id, request.referer)
      end
      if CustomWizard::Template.exists?(wizard_id)
        puts "33333333"
        redirect_to "/w/#{wizard_id.dasherize}"
      end
    end
  end

  add_to_serializer(:site, :include_wizard_required?) do
    scope.is_admin? && Wizard.new(scope.user).requires_completion?
  end

  add_to_serializer(:site, :complete_custom_wizard) do
    if scope.user && requires_completion = CustomWizard::Wizard.prompt_completion(scope.user)
      requires_completion.map { |w| { name: w[:name], url: "/w/#{w[:id]}" } }
    end
  end

  add_to_serializer(:site, :include_complete_custom_wizard?) do
    complete_custom_wizard.present?
  end

  add_model_callback(:application_controller, :before_action) do
    redirect_to_wizard_if_required if current_user
  end

  ::ExtraLocalesController.prepend ExtraLocalesControllerCustomWizard
  ::InvitesController.prepend InvitesControllerCustomWizard
  ::UsersController.prepend CustomWizardUsersController
  ::Wizard::Field.prepend CustomWizardFieldExtension
  ::Wizard::Step.prepend CustomWizardStepExtension

  full_path = "#{Rails.root}/plugins/expeerience_custom_wizard/assets/stylesheets/wizard/wizard_custom.scss"
  if Stylesheet::Importer.respond_to?(:plugin_assets)
    Stylesheet::Importer.plugin_assets['wizard_custom'] = Set[full_path]
  else
    # legacy method, Discourse 2.7.0.beta5 and below
    DiscoursePluginRegistry.register_asset(full_path, {}, "wizard_custom")
    Stylesheet::Importer.register_import("wizard_custom") do
      import_files(DiscoursePluginRegistry.stylesheets["wizard_custom"])
    end
  end

  CustomWizard::CustomField::CLASSES.keys.each do |klass|
    add_model_callback(klass, :after_initialize) do
      if CustomWizard::CustomField.enabled?
        CustomWizard::CustomField.list_by(:klass, klass.to_s).each do |field|
          klass.to_s
            .classify
            .constantize
            .register_custom_field_type(field[:name], field[:type].to_sym)
        end
      end
    end

    klass.to_s.classify.constantize.singleton_class.prepend CustomWizardCustomFieldPreloader
  end

  CustomWizard::CustomField.serializers.each do |serializer_klass|
    "#{serializer_klass}_serializer".classify.constantize.prepend CustomWizardCustomFieldSerializer
  end

  DiscourseEvent.trigger(:custom_wizard_ready)
end
