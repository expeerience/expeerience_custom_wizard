# frozen_string_literal: true

if ENV['SIMPLECOV']
  require 'simplecov'

  SimpleCov.start do
    root "plugins/expeerience_custom_wizard"
    track_files "plugins/expeerience_custom_wizard/**/*.rb"
    add_filter { |src| src.filename =~ /(\/spec\/|\/db\/|plugin\.rb|api)/ }
    SimpleCov.minimum_coverage 80
  end
end

require 'rails_helper'
