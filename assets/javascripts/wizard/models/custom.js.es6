import { default as computed } from 'discourse-common/utils/decorators';
import getUrl from 'discourse-common/lib/get-url';
import WizardField from 'wizard/models/wizard-field';
import { ajax } from 'wizard/lib/ajax';
import Step from 'wizard/models/step';
import EmberObject from "@ember/object";

const CustomWizard = EmberObject.extend({
  @computed('steps.length')
  totalSteps: length => length,

  skip() {
    if (this.required && (!this.completed && this.permitted)) return;
    CustomWizard.skip(this.id);
  },
});

CustomWizard.reopenClass({
  skip(wizardId) {
    ajax({ url: `/w/${wizardId}/skip`, type: 'PUT' }).then((result) => {
      CustomWizard.finished(result);
    });
  },

  finished(result) {
    let url = "/";
    if (result.redirect_on_complete) {
      url = result.redirect_on_complete;
    }
    console.log("WONDER WOMAN WONDER WOMAN WONDER WOMAN WONDER WOMAN WONDER WOMAN ");
    console.log();
    window.location.href = getUrl(url);
  }
});

export function findCustomWizard(wizardId, params = {}) {
  console.log("Hello World");
  let url = `/w/${wizardId}`;
  // let has_draft_url = `/w/${wizardId}/has_draft`;
  console.log("HAHAHAHAHHAAH 1");
  console.log(params);
  var category_id = document.getElementById("custom-wizard-main").getAttribute('category_id');
  params["category_id"] = category_id;
  console.log(params);
  let paramKeys = Object.keys(params).filter(k => {
    if (k === 'wizard_id') return false;
    return !!params[k];
  });

  console.log("HAHAHAHAHHAAH 2");
  console.log(category_id);
  console.log(paramKeys);

  if (paramKeys.length) {
    url += '?';
    // has_draft_url += '?';
    console.log("HAHAHAHAHHAAH 3");
    paramKeys.forEach((k,i) => {
      if (i > 0) {
        url += '&';
        // has_draft_url += '&';
      }
      url += `${k}=${params[k]}`;
      // has_draft_url += `${k}=${params[k]}`;
    });
  }

  console.log("HAHAHAHAHHAAH 4");
  console.log(url);

  // console.log("TRYING TO CHECK HAS_DRAFT " + has_draft_url);
  // var has_draft = ajax({has_draft_url, cache: false, dataType: 'json' }).then(result => {
  //   console.log("AFTER AJAX OF HAS_DRAFT")
  //   console.log(result)
  // });
  // console.log("RESULT:");
  // console.log(has_draft);

// return "Hey";

  return ajax({ url, cache: false, dataType: 'json' }).then(result => {
    console.log("HAHAHAHAHHAAH 5");
    console.log(result)
    const wizard = result;
    if (!wizard) return null;
    console.log("HAHAHAHAHHAAH 6");

    if (!wizard.completed) {
      wizard.steps = wizard.steps.map(step => {
        const stepObj = Step.create(step);
        console.log("HAHAHAHAHHAAH 7");
        
        stepObj.fields.sort((a, b) => {
          return parseFloat(a.number) - parseFloat(b.number);
        });
        console.log("HAHAHAHAHHAAH 8");
        
        let tabindex = 1;
        stepObj.fields.forEach((f, i) => {
          f.tabindex = tabindex;
          
          if (['date_time'].includes(f.type)) {
            tabindex = tabindex + 2;
          } else {
            tabindex++;
          }
        });
        console.log("HAHAHAHAHHAAH 9");
        
        stepObj.fields = stepObj.fields.map(f => WizardField.create(f));
        console.log("HAHAHAHAHHAAH 10");
        console.log(stepObj)
        
        return stepObj;
      });
    }

    if (wizard.categories) {
      let subcatMap = {};
      let categoriesById = {};
      let categories = wizard.categories.map(c => {
        if (c.parent_category_id) {
          subcatMap[c.parent_category_id] =
            subcatMap[c.parent_category_id] || [];
          subcatMap[c.parent_category_id].push(c.id);
        }
        return (categoriesById[c.id] = EmberObject.create(c));
      });

      // Associate the categories with their parents
      categories.forEach(c => {
        let subcategoryIds = subcatMap[c.get("id")];
        if (subcategoryIds) {
          c.set("subcategories", subcategoryIds.map(id => categoriesById[id]));
        }
        if (c.get("parent_category_id")) {
          c.set("parentCategory", categoriesById[c.get("parent_category_id")]);
        }
      });

      Discourse.Site.currentProp('categoriesList', categories);
      Discourse.Site.currentProp('sortedCategories', categories);
      Discourse.Site.currentProp('listByActivity', categories);
      Discourse.Site.currentProp('categoriesById', categoriesById);
      Discourse.Site.currentProp('uncategorized_category_id', wizard.uncategorized_category_id);
    }

    return CustomWizard.create(wizard);
  });
};

export default CustomWizard;
