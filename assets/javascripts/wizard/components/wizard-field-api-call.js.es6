import { action } from "@ember/object";
import { observes } from 'discourse-common/utils/decorators';
import Component from "@ember/component";
import { ajax } from "../lib/custom-ajax";
import discourseComputed from "discourse-common/utils/decorators";

export default Component.extend({
    value: null,
    error: null,

    didInsertElement() {
        const property = this.field.property || 'id';
    },

    @discourseComputed("field.content[].name")
    updatefeild(){

    },

    @observes('value')
    setField() {
        let mapFields = this.field.map_response
        let fields = this.step.fields
        let list = []
        let test = this.value['productionunits.0.address']
        mapFields.forEach(mapField => {
            let arr = {}
            for(var i in fields ){
                if(i != '_super'){
                    let label = fields[i].label.replace('<p>', '').replace('</p>','')

                    if(mapField.name == label ){
                        arr.index = i
                        arr.value = this.value[mapField.id]
                    }
                }
            }
        
            
            list.push(arr)
        });

       for(var n of list ){
            this.set(`step.fields.${n.index}.value`, n.value);
       }

    },

    @action
    submit(){
        let field = this.field.content
       
        let url = this.field.api
        let data = {}
        for (var i of field){
            data[i.id] = i.name; 
        }

        ajax(url,{
            type: "GET",
            data
        }).then(res => {

            if( !res.error){
                this.set('value',res)
                this.set('error', null)
            } else {

                this.set('error', res.message)
            }
        })
    },
});