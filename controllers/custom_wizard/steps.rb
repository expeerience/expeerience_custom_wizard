# frozen_string_literal: true
class CustomWizard::StepsController < ::ApplicationController
  before_action :ensure_logged_in
  # before_action :ensure_can_update

  def update
    category_id = params[:fields][:category_id]
    puts "STEP UPDATE #{category_id} START"
    ensure_can_update(category_id)
    puts params.inspect
    params.require(:step_id)
    params.require(:wizard_id)

    wizard = @builder.build()
    step = wizard.steps.select { |s| s.id == update_params[:step_id] }.first

    raise Discourse::InvalidParameters.new(:step_id) if !step

    update = update_params.to_h

    update[:fields] = {}
    if params[:fields]
      field_ids = step.fields.map(&:id)
      params[:fields].each do |k, v|
        update[:fields][k] = v if field_ids.include? k
      end
    end

    puts "UPDATE UPDATE HELLO #{category_id}"
    puts update.inspect
    puts "THIS IS THE WIZARD actions"
    puts wizard.actions.inspect
    updater = wizard.create_updater(update[:step_id], update[:fields])
    puts "From StepsController.update"
    updater.update # <-- this is where the topic creation is done.

    if updater.success?
      result = success_json
      result.merge!(updater.result) if updater.result
      result[:refresh_required] = true if updater.refresh_required?

      render json: result
    else
      errors = []
      updater.errors.messages.each do |field, msg|
        errors << { field: field, description: msg.join(',') }
      end
      render json: { errors: errors }, status: 422
    end
    puts "END OF STEP UPDATE"
  end

  def saveFields
    category_id = params[:fields][:category_id]
    puts "SAVE FIELDS API START #{category_id}"

    ensure_can_update(category_id)
    puts params.inspect
    params.require(:step_id)
    params.require(:wizard_id)

    wizard = @builder.build()
    step = wizard.steps.select { |s| s.id == update_params[:step_id] }.first

    raise Discourse::InvalidParameters.new(:step_id) if !step

    update = update_params.to_h

    update[:fields] = {}
    if params[:fields]
      field_ids = step.fields.map(&:id)
      params[:fields].each do |k, v|
        update[:fields][k] = v if field_ids.include? k
      end
    end

    puts "UPDATE UPDATE HELLO #{category_id}"
    puts update.inspect
    puts "THIS IS THE WIZARD actions"
    puts wizard.actions.inspect
    updater = wizard.create_updater(update[:step_id], update[:fields], true)
    puts "UPDATER.UPDATE START"
    updater.updateFields # <-- this is where the topic creation is done.
    # 
    puts "UPDATER.UPDATE END"

    if updater.success?
      result = success_json
      result.merge!(updater.result) if updater.result
      result[:refresh_required] = true if updater.refresh_required?

      render json: result
    else
      errors = []
      updater.errors.messages.each do |field, msg|
        errors << { field: field, description: msg.join(',') }
      end
      render json: { errors: errors }, status: 422
    end
    puts "SAVE FIELDS API END"
  end

  private

  def ensure_can_update(category_id)
    @builder = CustomWizard::Builder.new(
      update_params[:wizard_id].underscore,
      current_user,
      category_id
    )

    if @builder.nil?
      raise Discourse::InvalidParameters.new(:wizard_id)
    end

    if !@builder.wizard || !@builder.wizard.can_access?
      raise Discourse::InvalidAccess.new
    end
  end

  def update_params
    params.permit(:wizard_id, :step_id)
  end
end
